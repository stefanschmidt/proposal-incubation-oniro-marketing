# Running an OpenThread Mesh Network with Linux and Zephyr

## Track
Embedded, Mobile and Automotive devroom

https://fosdem.org/2022/schedule/track/embedded_mobile_and_automotive/#

## Title
Running an OpenThread Mesh Network with Linux and Zephyr

## Subtitle

## Event type
20 min talk + 5 min QA

## Persons
Stefan Schmidt

## Abstract
The Thread protocol specifies a low-power mesh network on top of the IEEE
802.15.4 standard. In addition to the lower layer mesh functionality it also
offers mesh network management, as well as, secure onboarding of headless
devices.

OpenThread is an open source project to implement the Thread protocol and its
components. The focus of this talk is on a Linux based OpenThread border
router and Zephyr based mesh nodes. Tight together by a Yocto based build
system this talk demonstrates all components you need to have an IPv6 enabled
MCU on a low-power wireless link. The used power is small enough to allow
operating a small sensor for month or years on a coin cell battery in such a
scenario. All served by a Linux based border router to allow for internet
access and end-to-end IPv6 connectivity.

All of the above is bundled together in an Eclipse Oniro Project blueprint
for a transparent IoT gateway.

## Links
Blueprint documentaion:
https://docs.oniroproject.org/projects/blueprints/en/latest/transparent-gateway.html

Demonstration video:
https://www.youtube.com/watch?v=o_3ITbSAvNg
