# Oniro WG Incubation stage marketing 2021wk50

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2021-12-15

## Participants

(add/erase participants)

Shanda (EF), Yves (EF), Agustin B.B.(EF), Gael (EF), Adrian (Huawei), Chiara (Huawei), Patrick O. (NOI), Dony (Huawei), Aurore P. (Huawei), Andrea G. (Linaro) 

## Agenda

* Events list - Chiara 15 min
* Demonstrator vs Blueprint - Agustin 5 min
* Demonstrators: definition and roadmap. - Adrian 5 min
* AOB - 5 min

## MoM

### Events list

Presentation

* <Description>
* <Description> #link <link>

Discussion

* <Description>
* <Description> #link <link>


### Demonstrator vs Blueprint

Presentation

* <Description>
* <Description> #link <link>

Discussion

* <Description>
* <Description> #link <link>

### Demonstrators: definition and roadmap

Presentation

* <Description>
* <Description> #link <link>

Discussion

* <Description>
* <Description> #link <link>



### AOB

<Topic>

* <Description>
* <Description> #link <link>

Topics for next week

* <Topic> - <Lead pname>
* <Topic> - <Lead pname>


## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
